Vertical Tabs: Smart & Responsive 
----------------------------------
I couldn't find a good responsive vertical tabs solution, so...
I've came up with a better way to have tabs in order, and it's responsive. :)


A [Pen](https://codepen.io/jstgermain/pen/rWaEjp) by [Justin St Germain](http://codepen.io/jstgermain) on [CodePen](http://codepen.io/).

[License](https://codepen.io/jstgermain/pen/rWaEjp/license).